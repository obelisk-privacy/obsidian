// Retrieve environment variables
require('dotenv').config();

// Keep the bot online
const express = require("express");
const app = express();

app.listen(() => console.log("Server started"));

app.use('/', (req, res) => {
  res.send(new Date());
});

const pf = '~'; // Define prefix
const memberCount = '728042995659702313';
const welcome = '635676228287725588';

const verifyMsg = '689325025651720225';
const communityRole = '630980040263204894';

// Initialize the bot
const Discord = require('discord.js');
const bot = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

const isMod = (member) => member.roles.cache.some(role => role.name === 'Moderator');

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}!`);
});

// Commands
bot.on('message', message => {
	if(message.content.startsWith(pf) && !message.author.bot) {
		const reply = (msg) => message.channel.send(msg);
		const content = message.content.substr(1);

		const parts = content.split(' ');

		switch (parts[0]) {
			case 'hi':
				reply('Hi!');
				break;

			case 'help':
				message.channel.send({
					embed: {
						color: 0xffac37,
						title: 'Obisidan Commands',
						fields: [
							{
								name: 'Utilities',
								value: '• `~help`\n• `~ping`\n• `~members`',
							},
							{
								name: 'Moderation (requires Mod role)',
								value: '• `~prune`\n• `~welcome`',
							},
							{
								name: 'Fun',
								value: '• `~hi`\n• `~borat`'
							}
						]
					}
				});
				break;

			case 'prune':
				if(!isMod(message.member)) { return reply('🙈 Invalid permisisons.') }
				if(parts.length != 2) { return reply(`😩 ${pf}prune takes one parameter.`) }
				if(isNaN(parts[1])) { return reply(`🔢 First parameter should be an integer.`) }
				message.channel.bulkDelete(parts[1], true);
				reply(`🗑 ${parts[1].toString()} messages deleted.`).then(m => setTimeout(() => m.delete(), 2500));
				break;

			case 'welcome':
				if(!isMod(message.member)) { return reply('🙈 Invalid permisisons.') }
				message.delete();
				message.channel.send({
					embed: {
						color: 0xffac37,
						title: 'Welcome to Obelisk!',
						description: 'Privacy and security community building free open source software.',
						fields: [
							{
								name: 'Links',
								value: '🔹 Gitlab Page: https://gitlab.com/obelisk-privacy\n🔹 Bot: https://gitlab.com/obelisk-privacy/obsidian',
							},
							{
								name: 'Rules',
								value: '🔸 Respect everyone, no gatekeeping.\n🔸 No advertising.\n🔸 Follow Discord Guidelines (https://discordapp.com/guidelines)'
							}
						],
						timestamp: new Date(),
						image: {
							url: 'https://i.ibb.co/YRvgd7S/Obelisk-Banner.png',
						}
					}
				});
				break;

			case 'ping':
				reply(`🏓 **Pong!** Took ${Date.now() - message.createdTimestamp}ms`);
				break;

			case 'borat':
				reply('https://tenor.com/wpmn.gif');
				break;

			case 'members':
				let count = message.channel.guild.members.cache.filter(m => !m.user.bot).size;
				reply(`👪 Obelisk has ${count} members.`);
				break;

			default:
				break;
		}
	}
});

const welcomeMessages = [
	"Welcome %s to Obelisk!",
	"Thanks for joining Obelisk, %s!",
	"Privacy awaits you, %s, here in Obelisk.",
	"Looking to stay secure? You're in the right place %s.",
	"Hey %s! We're glad you've showed up",
	"%s Greetings traveller! First stop, Obelisk"
];

// Welcome message
bot.on('guildMemberAdd', m => {
	bot.channels.fetch(welcome).then(c => {
		var randomNumber = Math.floor(Math.random()*welcomeMessages.length);
		var weclomeMsg = welcomeMessages[randomNumber].replace('%s', `<@${m.id}>`);
		c.send(`<:obelisk:729513834057957377> ${weclomeMsg}`);
	});
});

// Leave message
bot.on('guildMemberRemove', async m => {
	bot.channels.fetch(welcome).then(c => {
		c.send(`👋 Farewell ${m.user.tag}.`);
	});
});

// Reaction added
bot.on('messageReactionAdd', (message_id, reaction, user) => {
	if(message_id != verifyMsg) return;
	let member = bot.guilds.cache.array()[0].member(user);
	member.roles.add(communityRole);
});

bot.on('raw', event => {
	if(event.t == 'MESSAGE_REACTION_ADD') {
		bot.users.fetch(event.d.user_id).then(user => {
			const emoji = event.d.emoji.id ? `${event.d.emoji.name}:${event.d.emoji.id}` : event.d.emoji.name;
			bot.emit('messageReactionAdd', event.d.message_id, emoji, user);
		});
	}
});

bot.login(process.env.token);
